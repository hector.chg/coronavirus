#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# %% import librerías

# Terminal I/O
import sys
from os import getcwd
import json

# Data Processing
import numpy as np
import pandas as pd
# import statsmodels as sm
from scipy.optimize import curve_fit
## Date & time
import datetime as dt
# import pytz  # timezones
from timeit import default_timer as timer

# DataViz
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

plt.style.use('dark_background')

# MyModules
from modules import extract
from modules import processing_functions as SP

# %% Workspace
wspace = getcwd()
datadir = wspace + "/data/"
resultsdir = wspace + "/results/"

# %% (Extract) Select country and get it's data
country = sys.argv[1]
country = country.capitalize()
print(f"Reading {country}...")

metaparameters=extract.read_json(datadir+"metaparameters.json")

country_metaparameters = metaparameters[country]

dirty_data = extract.get_wikidata_for_single_country(country, country_metaparameters['URL'], country_metaparameters['RegEx'])

clean_data = extract.clean_spain(dirty_data)

# %% Save dataframes as csv's
now = dt.datetime.today().strftime("%d%b")
csvfilename = datadir + country + "Cleaned.csv"
clean_data.to_csv(csvfilename, index=True)
print(f"Cleaned data at {csvfilename}, at {now}")


# %% curvefit to generalised logistic regression
timespan = pd.date_range(start=clean_data.index[0], end=clean_data.index[-1])
x = mdates.date2num(timespan)

col_labels = ("Saturation", "Growth rate", "Asymmetry", "Offset")
indexes = ("Parameters", "Errors")

start = timer()
# RIPS
seed_dead = [30000, 0.1, 0.01, x[0]]
bounds_dead = ([0, 0, 0, 0], [country_metaparameters["Population"], 1, 1, 1000000])
dead_df, r2_dead = SP.fit(x, clean_data['Deaths'], SP.generalised_logistic, 
                        seed_dead, bounds_dead, indexes, col_labels)
# RECOVERIES
seed_rec = [100000, 0.1, 0.3, x[0]]
bounds_rec = ([0, 0, 0, 0], [country_metaparameters["Population"], 3, 3, 1000000])
rec_df, r2_rec = SP.fit(x, clean_data['Recoveries'], SP.generalised_logistic, 
                        seed_rec, bounds_rec, indexes, col_labels)
# TOTAL CASES
seed_total = [240000, 0.08, 0.006, 50+x[0]]
bounds_total = ([0, 0, 0, 0], [country_metaparameters["Population"], 3, 3, 1000000])
total_df, r2_total = SP.fit(x, clean_data['Total Infected'], SP.generalised_logistic, 
                            seed_total, bounds_total, indexes, col_labels)
# DIFF RIPS
seed_diff_rip = (dead_df.to_numpy()[:, 0])
bounds_diff_rip = ([0, 0, 0, 0],
                    [country_metaparameters["Population"]*0.035, 1, 1, 1000000])
diff_dead_df, r2_diff_dead = SP.fit(x, clean_data['Diff Deaths'],
                        SP.derivative_gl, seed_diff_rip, bounds_diff_rip,
                        indexes, col_labels)
# DIFF RECOVERIES
seed_diff_rec = (rec_df.to_numpy()[:, 0])
bounds_diff_rec = ([0, 0, 0, 0],
                    [country_metaparameters["Population"], 1, 1, 1000000])
diff_rec_df, r2_diff_rec = SP.fit(x, clean_data['Diff Recoveries'],
                        SP.derivative_gl, seed_diff_rip, bounds_diff_rip,
                        indexes, col_labels)
# DIFF TOTAL CASES
seed_diff_total = (total_df.to_numpy()[:, 0])
bounds_diff_total = bounds_diff_rec
diff_total_df, r2_diff_total = SP.fit(x, clean_data['Diff Total'],
                                    SP.derivative_gl, seed_diff_rip, bounds_diff_total,
                                    indexes, col_labels)
end = timer()
del(seed_dead, seed_rec, seed_total, seed_diff_rip, seed_diff_rec, seed_diff_total)
del(bounds_dead, bounds_rec, bounds_total, bounds_diff_rip, bounds_diff_rec, bounds_diff_total)
time_elapsed = end - start
print("\n\nFitting times: %s seconds." % time_elapsed)
del(start, end)

# %% testing
# ToDo: rename indexes
r2_df = pd.DataFrame(data=(r2_dead, r2_rec, r2_total, 
                            r2_diff_dead, r2_diff_rec, r2_diff_total))

multiindex = pd.MultiIndex.from_product([clean_data.columns, indexes], names=['Serie', 'Value'])
model_parameters_df = pd.concat([dead_df, rec_df, total_df, 
                                    diff_dead_df, diff_rec_df, diff_total_df], 
                                axis=1, sort=False)
model_parameters_df.columns = multiindex

# %% Confinement predictions

# Start Of State Of Alarm (format: remove tz info & parse datetime)
so_soa_datetime = dt.datetime.strptime(country_metaparameters["Start of SOA"], "%d/%m/%Y").replace(tzinfo=None)
# Start Of State Of Alarm (datetime to num)
so_soa_num = mdates.date2num(so_soa_datetime)

# How many cases on the start of state of alarm?
f = SP.generalised_logistic(so_soa_num, *total_df.to_numpy()[:, 0])

# The end of SOA will be the day that has the saturation minus the cases there were on the start
expected_num_of_eo_soa = SP.solved_generalised_logistic(total_df.iloc[0, 0]-f/country_metaparameters["Conservadurism"], *total_df.to_numpy()[:, 0])
expected_eo_soa = mdates.num2date(expected_num_of_eo_soa).replace(tzinfo=None)

# Which day will we be free?
expected_endofconfination = dt.datetime.strftime(expected_eo_soa, "%d%B")

# How many total days in SOA?
expected_time_in_confinement = (expected_eo_soa - so_soa_datetime).days

# delete dummy variables
del(f, so_soa_num)

# %% wrangling with models
ncases = len(clean_data)

# declare x axis for the models
x_plot = np.linspace(x[0], expected_num_of_eo_soa+5, 24*ncases)

# compute y
y_total = SP.generalised_logistic(x_plot, *total_df.to_numpy()[:, 0])
y_rec = SP.generalised_logistic(x_plot, *rec_df.to_numpy()[:, 0])
y_death = SP.generalised_logistic(x_plot, *dead_df.to_numpy()[:, 0])

y_total_diff = SP.derivative_gl(x_plot, *diff_total_df.to_numpy()[:, 0])
y_rec_diff = SP.derivative_gl(x_plot, *diff_rec_df.to_numpy()[:, 0])
y_dead_diff = SP.derivative_gl(x_plot, *diff_dead_df.to_numpy()[:, 0])


dd = mdates.num2date(x_plot)
data = np.array((dd, y_death, y_rec, y_total, 
                    y_dead_diff, y_rec_diff, y_total_diff)).T

results_df = pd.DataFrame(data).set_index(0)
results_df.columns = clean_data.columns

# delete dummy variables
del(data, y_total, y_rec, y_death, y_dead_diff, y_rec_diff, y_total_diff)

# %% presenting results througout strings

bar = 79*"-"

text_totalcases_model = """Total logistic model:\t\t r²: %1.8f
\tExpected:\t %1.0f \u00B1 %1.0f total expected total cases
\tGrowth rate:\t %1.3f \u00B1 %1.3f growth
\tAsymmetry:\t %1.2f \u00B1 %1.2f days since first case
\tOffset:\t\t %s \u00B1 %1.0f days since first case""" % (
r2_total,
total_df.iloc[0, 0], total_df.iloc[0, 1],
total_df.iloc[1, 0], total_df.iloc[1, 1],
total_df.iloc[2, 0], total_df.iloc[2, 1],
mdates.num2date(total_df.iloc[3, 0]).strftime("%d-%b"), total_df.iloc[3, 1])
print(bar, "\n", text_totalcases_model)

text_recoveries_model = """
Recoveries logistic model:\t r²: %1.8f
\tExpected:\t %1.0f \u00B1 %1.0f total expected recoveries
\tGrowth rate:\t %1.3f \u00B1 %1.3f growth
\tAsymmetry:\t %1.2f \u00B1 %1.2f days since first case
\tOffset:\t\t %s \u00B1 %1.0f days since first case
""" % (
r2_rec,
rec_df.iloc[0, 0], rec_df.iloc[0, 1],
rec_df.iloc[1, 0], rec_df.iloc[1, 1],
rec_df.iloc[2, 0], rec_df.iloc[2, 1],
mdates.num2date(rec_df.iloc[3, 0]).strftime("%d-%b"), rec_df.iloc[3, 1])
print(text_recoveries_model)

text_death_model = """Death logistic model:\t\t r²: %1.8f
\tExpected:\t %1.0f \u00B1 %1.0f total expected deaths
\tGrowth rate:\t %1.3f \u00B1 %1.3f growth
\tAsymmetry:\t %1.4f \u00B1 %1.4f days since first case
\tOffset:\t\t %s \u00B1 %1.0f days since first case"""  % (
r2_dead,
dead_df.iloc[0, 0], dead_df.iloc[0, 1],
dead_df.iloc[1, 0], dead_df.iloc[1, 1],
dead_df.iloc[2, 0], dead_df.iloc[2, 1],
mdates.num2date(dead_df.iloc[0, 0]).strftime("%d-%b"), dead_df.iloc[0, 1])
print(text_death_model)


diff_infected = SP.signify(clean_data['Total Infected'][-1]-clean_data['Total Infected'][-2])
infectedtoday = clean_data['Total Infected'][-1]
infectedtomorrow = SP.generalised_logistic(x[-1]+1, *total_df.to_numpy()[:, 0])
err_infectedtomorrow = infectedtomorrow*total_df.iloc[0, 1]/total_df.iloc[0, 0]
diff_infectedtomorrow = SP.signify(infectedtomorrow - infectedtoday)

diff_dead = SP.signify(int(clean_data['Deaths'][-1]-clean_data['Deaths'][-2]))
deadtoday = clean_data['Deaths'][-1]
deadtomorrow = SP.generalised_logistic(x[-1]+1, *dead_df.to_numpy()[:, 0])
err_deadtomorrow = deadtomorrow * dead_df.iloc[0, 1]/dead_df.iloc[0, 0]
diff_deadtomorrow = SP.signify(deadtomorrow-deadtoday)

diff_rec = SP.signify(int(clean_data['Recoveries'][-1]-clean_data['Recoveries'][-2]))
rectoday = clean_data['Recoveries'][-1]
rectomorrow = SP.generalised_logistic(x[-1]+1, *rec_df.to_numpy()[:, 0])
err_rectomorrow = rectomorrow * rec_df.iloc[0, 1]/rec_df.iloc[0, 0]
diff_rectomorrow = SP.signify(int(rectomorrow-rectoday))

text_results = """%s
Cases yesterday:\t\t%1.0f
Cases today:\t\t\t%1.0f\t\t(%s)
Expected cases for tomorrow:\t%1.0f \u00B1 %1.0f\t(%s)
-------------------------------------------
Deaths yesterday:\t\t%1.0f
Deaths today:\t\t\t%1.0f\t\t(%s)
Expected deaths for tomorrow:\t%1.0f \u00B1 %1.0f\t(%s)
-------------------------------------------
Recoveries yesterday:\t\t\t%1.0f
Recoveries today:\t\t\t%1.0f\t\t(%s)
Expected recoveries for tomorrow:\t%1.0f \u00B1 %1.0f\t(%s)
%s
""" % (bar,
        clean_data['Total Infected'][-2],
        clean_data['Total Infected'][-1], diff_infected,
        infectedtomorrow, err_infectedtomorrow, diff_infectedtomorrow,
        clean_data['Deaths'][-2],
        clean_data['Deaths'][-1], diff_dead,
        deadtomorrow, err_deadtomorrow, diff_deadtomorrow,
        clean_data['Recoveries'][-2],
        clean_data['Recoveries'][-1], diff_rec,
        rectomorrow, err_rectomorrow, diff_rectomorrow,
        bar)
print(text_results)


text_stats = """Current expected mortality:\t%1.2f %%

Total expected time of confinement:\t\t%1.0f days
Total expected time left of confinement:\t%1.0i days (%s)

Total infected following IMM results for Spain
    as of most recent update:\t\t%1.0f people
""" % (100*dead_df.iloc[0, 0]/total_df.iloc[0, 0],
        expected_time_in_confinement,
        expected_num_of_eo_soa-x[-1], expected_endofconfination,
        round(dead_df.iloc[0, 0]/(0.05*0.065), -3))
print(text_stats)


subplot1_text = """First registered case on %s.
Last updated on %s.""" % (
dt.datetime.strftime(dd[0], '%d of %B, %Y'),
dt.datetime.strftime(clean_data.index[-1], '%d of %B, %Y'))


subplot2_text = """First registered case on %s.
Last updated on %s.""" % (
dt.datetime.strftime(dd[0], '%d of %B, %Y'),
dt.datetime.strftime(clean_data.index[-1], '%d of %B, %Y'))

model = r'$f(t) = \frac{f_{Max}}{\left(1+e^{-\beta(t-t_0)}\right)^{1/\nu}}$'

derivativemodel = r"$f'(t) = -f(t)\frac{\beta/\nu}{\left( 1 + e^{-\beta(t-t_0)}\right)^{\nu-1}}$"

infotext_cumulative = """
""" % ()

# %% plotting
plt.rc("text", usetex=True)

figtitle = country + ".svg"

fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(15, 10), sharex=True)

plt.suptitle("%s's COVID-19 crisis. Forecasts." % (country), fontsize=18)

# CUMULATIVES
plt.subplot(211)
plt.title("Cumulative curves.", x=0.06)

# Raw Data
plt.bar(clean_data.index, clean_data['Total Infected'], color="r", alpha=0.15)
plt.bar(clean_data.index, clean_data['Recoveries'], color="lime", alpha=0.15)
plt.bar(clean_data.index, clean_data['Deaths'], color="w", alpha=0.40)
# Fittings
plt.plot(results_df.index, results_df['Total Infected'],
            "r", linewidth=3, alpha=0.3,
            label="Total cases   %1.6f" % r2_total)
plt.plot(results_df.index, results_df['Recoveries'],
            "lime", linewidth=3, alpha=0.3,
            label="Recoveries %1.6f" % r2_rec)
plt.plot(results_df.index, results_df['Deaths'],
            "w", linewidth=3, alpha=0.45,
            label="Deaths\t%1.6f" % r2_dead)
#SOA
plt.axvspan(so_soa_datetime, expected_eo_soa, facecolor='grey', 
            alpha=0.15, label="Expected time of confinement")

plt.legend(loc="upper left", frameon=False)
plt.text(0.15, 0.98, model, transform=ax1.transAxes, fontsize=12)


# DIFFS
plt.subplot(212)
plt.title("Diary evolution.", x=0.06)
# Bars
plt.bar(clean_data.index, clean_data['Diff Total'], color="r", alpha=0.15,)
plt.bar(clean_data.index, clean_data['Diff Recoveries'], color="lime", alpha=0.15)
plt.bar(clean_data.index, clean_data['Diff Deaths'], color="white", alpha=0.40)
# Fits
plt.plot(results_df.index, results_df['Diff Total'],
            "r", linewidth=3, alpha=0.3,
            label="Total cases   %1.6f" % r2_diff_total)
plt.plot(results_df.index, results_df['Diff Recoveries'],
            "lime", linewidth=3, alpha=0.3,
            label="Recoveries %1.6f" % r2_diff_rec)
plt.plot(results_df.index, results_df['Diff Deaths'],
            "w", linewidth=3, alpha=0.45,
            label="Deaths\t%1.6f" % r2_diff_dead)
# SOA
plt.axvspan(so_soa_datetime, expected_eo_soa, facecolor='grey', alpha=0.15)
plt.legend(loc="upper left", frameon=False)
plt.text(0.13, 0.82, derivativemodel, transform=ax2.transAxes, fontsize=12)

fig.tight_layout(rect=[0, 0, 1, 1])
plt.savefig(resultsdir+figtitle, dpi=100)