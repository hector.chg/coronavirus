import numpy as np
from scipy.optimize import curve_fit
import pandas as pd

# actual mathematical functions
def generalised_logistic(x, *params):
    # asymmetric logistic function with one degree of freedom for asymmetry
    upper_asymptoteK, growth_rateB, assymetryNU, offsetQ = params
    denom = (1 + np.exp(-growth_rateB * (x - offsetQ))) ** (1 / assymetryNU)
    return upper_asymptoteK/denom

def solved_generalised_logistic(f, *params):
    upper_asymptoteK, growth_rateB, assymetryNU, offsetQ = params
    return offsetQ-np.log(((f/upper_asymptoteK)**(-assymetryNU))-1)/growth_rateB

def derivative_gl(x, *params):
    upper_asymptoteK, growth_rateB, assymetryNU, offsetQ = params
    num = generalised_logistic(x, *params) * growth_rateB / assymetryNU
    denom = 1 + np.exp(growth_rateB * (x - offsetQ))
    return num / denom


# i am lazy and i try to write things only once
def obtain_r_squared(xseries, yseries, model, *model_parameters):
    # explain this shit
    residuals = yseries - model(xseries, *model_parameters)
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((yseries-np.mean(yseries))**2)
    r_squared = 1 - (ss_res / ss_tot)
    return r_squared

def remove_error_outliers(parameters, cov_matrix):
    # explain this shit
    for i, item in enumerate(cov_matrix):
        if cov_matrix[i, i] > 20*parameters[i]:
            cov_matrix[i, i] = 0
    return np.sqrt(cov_matrix)

def solve_dates(date2num, error, firstvalue):
    # given matplotlib dates and its error, substracts first value, 
    # and decreases proportionaly its error.
    date2num_adjusted = date2num-firstvalue
    error_adjusted = date2num_adjusted * error / date2num
    return date2num_adjusted, error_adjusted

def signify(num):
    # explain this shit
    if num < 0:
        new_num = str(int(num))
    elif num > 0:
        new_num = "+"+str(int(num))
    else:
        new_num = str(int(num))
    return new_num

def fit(x, y, model, initial_guess, bounds, indexes, col_labels):
    popt, pcov = curve_fit(model, x, y,
                           initial_guess,
                           maxfev=10000000, bounds=bounds)
    pcov = remove_error_outliers(popt, pcov)
    r_squared = obtain_r_squared(x, y, model, *popt)
    popt = tuple(popt)
    pcov = tuple(np.diag(pcov))
    
    # Build Dataframe of the results
    df = pd.DataFrame(data=(popt, pcov),
                      index=indexes, columns=col_labels).T
    return df, r_squared