#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 18:56:55 2020

@author: hector
"""

# %% import modules
import re
import requests

import time
import datetime as dt

import pandas as pd
import numpy as np

import json
from os import getcwd

wspace = getcwd()
datadir = wspace + "/../data/"
resultsdir = wspace + "/../results/"

def read_json(file=datadir+"metaparameters.json"):
    try:
        print(f"Reading {file}...")
        with open(file, "r") as read_file:
            metaparameters = json.load(read_file)
        print(f"Success reading {file}\n")
        return metaparameters
    except:
        print(f"\tError reading {file}\n")


def get_wikidata_for_single_country(country, url, regex):
    print(f"Trying to retrieve Wikipedia data for {country}")
    try:
        url_code = requests.get(url).content.decode('utf-8')
        print("Success reading Wikipedia!\n")

        print("Now trying to parsing it...")
        try:
            wiki_data = re.findall(regex, url_code)
            
            if wiki_data == []:
                print(f"""\tError trying to parse {country}. 
                Change the regex using www.regex101.com\n\n""")
            else:
                print(f"Success parsing {country}!\n")
                return wiki_data
        except:
            print("\tSome error was found in regex\n")
    except:
        print(f"\tCould not read wikipedia, check URL for {country}")


def get_wikidata_for_all_countries(data_source=datadir+"metaparameters.json"):
    metadata = read_json(data_source)

    if metadata == "":
        return f"\tMetaparameters not found"

    for country, values in metadata.items():
        url = values['URL']
        regex = values['RegEx']
        
        get_wikidata_for_single_country(country, url, regex)


def save_MoMo_CSV(url="https://momo.isciii.es/public/momo/data"):
    now = dt.datetime.today().strftime("%d%B")
    csvname = datadir+"momo_raw_data"+".csv"
    
    start = time.time()
    df = pd.read_csv(url, encoding='utf-8')
    df.to_csv(csvname)
    end = time.time()
    elapsed = end - start
    
    print(f"Saved MoMo raw data as a CSV the {now} on {csvname}")
    print(f"Elapsed: {time}")
    return elapsed
    
def save_ISCII_CSV(url="https://covid19.isciii.es/resources/serie_historica_acumulados.csv"):
    now = dt.datetime.today().strftime("%d%B")
    csvname = datadir+"iscii_raw_data.csv"
    
    start = time.time()
    df = pd.read_csv(url, encoding='ISO-8859-1')
    df.to_csv(csvname)
    end = time.time()
    elapsed = end - start
    
    print(f"Saved ISCIII data as a CSV the {now} on {csvname}")
    print(f"Elapsed: {time}")
    return elapsed

def downloadALL():
    bar = 80*"-"
    print(bar)
    print("Downloading official numbers (ISCIII)")
    isciii_s = time.time()
    save_ISCII_CSV()
    isciii_e = time.time()
    isciii = isciii_e - isciii_s
    print(f"Official numbers downloaded in {isciii} seconds.\n")
    
    print("Downloading real deaths (MoMo)")
    momo = save_MoMo_CSV()
    print(f"Real numbers downloaded in {momo} seconds.\n")
    
    print("Downloading wikipedia data")
    wiki_s = time.time()
    get_wikidata_for_all_countries()
    wiki_e = time.time()
    wiki = wiki_e - wiki_s
    print(f"Wiki data downloaded in {wiki} seconds.\n")

def clean_spain(dirtydata):

    df = pd.DataFrame.from_dict(dirtydata)
    
    # 1) Build dataframe
    column_labels = ('Date', 'Deaths', 'Recoveries', 'Total Infected')
    df.columns = column_labels
    
    # 2) Flag and remove empty dates
    df['Date'].replace('', pd.NA, inplace=True)
    df.dropna(subset=['Date'], inplace=True)
    
    # 3) Format dates as datetime objects
    df['Date'] =  pd.to_datetime(df['Date'])
    
    # 4) Set correct index
    df = df.set_index('Date')
    df = df.fillna(0)
    
    # 5) Eval strings in Recoveries
    df[['aux1','aux2', 'aux3']] = df['Recoveries'].str.split('-', expand=True)
    df['aux2'].replace([None], 0, inplace=True)
    df['aux3'].replace([None], 0, inplace=True)
    df = df.replace('', '0')
    df.aux1 = pd.to_numeric(df.aux1, errors='coerce').astype(np.int64)
    df.aux2 = pd.to_numeric(df.aux2, errors='coerce').astype(np.int64)
    df.aux3 = pd.to_numeric(df.aux3, errors='coerce').astype(np.int64)
    df.aux1 = df.aux1 - df.aux2 - df.aux3
    df['Recoveries'] = df.aux1.values
    df = df.drop(columns=['aux1', 'aux2', 'aux3'])
    df.Deaths = pd.to_numeric(df.Deaths, errors='coerce').astype(np.int64)
    df['Total Infected'] = pd.to_numeric(df['Total Infected'], errors='coerce').astype(np.int64)
    
    # 5) Fill non-variation dates with previous data. This will increase the precision.
    timespan = pd.date_range(start=df.index[0], end=df.index[-1])
    df = df.reindex(timespan, fill_value=0)
    
    # 8) pandas is incredible: 
    # Fill those values whose indexes were not in the previous df with the previous 
    # non-zero value.
    df = df.replace(to_replace=0, method='ffill')
    
    # 9) Make the numbers integers and not strings
    df = df.astype(np.int64)
    
    # 10) Create a new dataframe for the differences
    # Change labels, replace the first row of diff with the cumulative df, and make 
    # numbers integers.
    diff_column_labels = ('Diff Deaths', 'Diff Recoveries', 'Diff Total')
    diff_df = df.diff()
    diff_df.columns = diff_column_labels
    diff_df.iloc[[0]] = df.iloc[[0]]
    diff_df = diff_df.astype(np.int64)
    
    # 11) Append new df to old df.
    df = pd.concat([df, diff_df], axis=1, sort=False)
    del(diff_df)

    return df

def clean(country):
    return 0